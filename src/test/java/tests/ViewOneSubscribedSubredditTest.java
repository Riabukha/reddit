package tests;

import core.BaseTest;
import org.junit.Test;
import pages.PostPage;
import pages.SubredditPage;
import utilities.StringValues;

public class ViewOneSubscribedSubredditTest extends BaseTest {

    SubredditPage page;

    @Test
    public void test() {
        page = new SubredditPage(getDriver());
        page.goToPage();
        page.clickMenuBtn()
                .clickCommunityOldSchool()
                .verifyCurrentUrl(StringValues.url);
    }
}
