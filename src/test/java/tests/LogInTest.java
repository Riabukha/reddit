package tests;

import core.BaseTest;
import org.junit.Test;
import pages.LogInPage;

public class LogInTest extends BaseTest {

    LogInPage page;

    @Test
    public void test() {
        page = new LogInPage(getDriver());
        page.goToPage();
        page.clickBtnLogIn()
                .enterUsername("")
                .enterPassword("")
                .clickSignIn()
                .verifyVisibilityGetCoinsBtn();
    }

}
