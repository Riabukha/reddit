package tests;

import core.BaseTest;
import org.junit.Before;
import org.junit.Test;
import pages.LogInPage;
import pages.PostPage;

public class DownvotePostTest extends BaseTest {

    PostPage page;
    LogInPage loginPage;

    @Before
    public void preconditionLogIn() {
        loginPage = new LogInPage(getDriver());
        loginPage.goToPage();
        loginPage.clickBtnLogIn()
                .enterUsername("Riabukha")
                .enterPassword("MyPassword")
                .clickSignIn()
                .verifyVisibilityGetCoinsBtn();
    }

    @Test
    public void test() {
        page = new PostPage(getDriver());
        page.goToPage();
        page.clickUserDropdownBtn()
                .clickMyProfile()
                .clickTabPost()
                .clickDownvoteBtn()
                .clickDownvotedTab()
                .verifyCommentVisible();
    }
}
