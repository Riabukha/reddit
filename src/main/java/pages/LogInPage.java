package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.junit.Assert;

public class LogInPage extends BasePage {

    By logInBtn = By.xpath("//a[contains(text(),'log in')]");
    By usernameField = By.xpath("//input[@id='loginUsername']");
    By passwordField = By.xpath("//input[@id='loginPassword']");
    By signInBtn = By.xpath("//button[@type='submit']");
    By getCoinsBtn = By.xpath("//*[@id=\"COIN_PURCHASE_DROPDOWN_ID\"]/div");

    public LogInPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl());
    }

    public LogInPage clickBtnLogIn() {
        getElement(logInBtn).click();
        return this;
    }

    public LogInPage enterUsername(String s) {
        getElement(usernameField).sendKeys(s);
        return this;
    }

    public LogInPage enterPassword(String s) {
        getElement(passwordField).sendKeys(s);
        return this;
    }

    public LogInPage clickSignIn() {
        getElement(signInBtn).click();
        return this;
    }

    public LogInPage verifyVisibilityGetCoinsBtn() {
        Assert.assertTrue("Element 'Get Coins' not found", getElement(getCoinsBtn).isExists());
        return this;
    }
}
