package pages;

import core.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PostPage extends BasePage {

    By userDropdownBtn = By.cssSelector(".\\_50RxI-5rW1xzwoC42vhzM");
    By myProfile = By.cssSelector(".\\_1YWXCINvcuU7nk0ED-bta8:nth-child(2) > .vzhy90YD0qH7ZDJi7xMGw");
    By tabPost = By.xpath("//a[contains(text(),'Posts')]");
    By commentLink = By.xpath("//div[4]/div[3]/a/span");
    By textCommentField = By.cssSelector(".public-DraftStyleDefault-block");
    By commentBtn = By.xpath("//button[@type='submit']");
    By publishedComment = By.cssSelector(".\\_1S45SPAIb30fsXtEcKPSdt");
    By upvoteBtn = By.xpath("//button[@id='upvote-button-t3_ebueee']/span/i");
    By downvoteBtn = By.xpath("//div[@id='t3_ebueee']/div/div/button[2]/span/i");
    By upvotedTab = By.xpath("//a[contains(text(),'Upvoted')]");
    By downvotedTab = By.xpath("//a[contains(text(),'Downvoted')]");
    By commentVisible = By.cssSelector(".\\_2XDITKxlj4y3M99thqyCsO");

    public PostPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl());
    }

    public PostPage clickUserDropdownBtn() {
        getElement(userDropdownBtn).click();
        return this;
    }

    public PostPage clickMyProfile() {
        getElement(myProfile).click();
        return this;
    }

    public PostPage clickTabPost() {
        getElement(tabPost).click();
        return this;
    }

    public PostPage clickCommentLink() {
        getElement(commentLink).click();
        return this;
    }

    public PostPage enterTextCommentField(String s) {
        getElement(textCommentField).sendKeys(s);
        return this;
    }

    public PostPage clickCommentBtn() {
        getElement(commentBtn).click();
        return this;
    }

    public PostPage verifyPublishedComment() {
        Assert.assertTrue("Published comment is absent", getElement(publishedComment).isExists());
        return this;
    }

    public PostPage clickUpvoteBtn() {
        getElement(upvoteBtn).click();
        return this;
    }

    public PostPage clickDownvoteBtn() {
        getElement(downvoteBtn).click();
        return this;
    }

    public PostPage clickUpvotedTab() {
        getElement(upvotedTab).click();
        return this;
    }

    public PostPage clickDownvotedTab() {
        getElement(downvotedTab).click();
        return this;
    }

    public PostPage verifyCommentVisible() {
        getElement(commentVisible).isExists();
        return this;
    }
}
