package pages;

import core.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SubredditPage extends BasePage {

    By menuBtn = By.xpath("//button[contains(.,'Home')]");
    By communityOldSchool = By.xpath("//span[contains(.,'r/OldSchoolRidiculous')]");

    public SubredditPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        getDriver().get(getBaseUrl());
    }

    public SubredditPage clickMenuBtn() {
        getElement(menuBtn).click();
        return this;
    }

    public SubredditPage clickCommunityOldSchool() {
        getElement(communityOldSchool).click();
        return this;
    }

    public SubredditPage verifyCurrentUrl(String url) {
        Assert.assertEquals(getDriver().getCurrentUrl(), url);
        return this;
    }
}
