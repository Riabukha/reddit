package core;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebElement;

public class BaseElement {
    protected WebElement webElement;

    public BaseElement(WebElement webElement) {
        this.webElement = webElement;
    }

    public void click() {
        try {
            webElement.click();
        }catch (ElementNotInteractableException e){

        }
    }

    public void sendKeys(String s) {
        webElement.sendKeys(s);
    }

    public boolean isExists() { return (webElement != null && webElement.isDisplayed());}

    public String getValue(){
        return webElement.getText();
    }

    public void clear() {webElement.clear();}

    public boolean isSelected() { return webElement.isSelected(); }
}
